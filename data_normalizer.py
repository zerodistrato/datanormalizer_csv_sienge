# -*- coding: utf-8 -*-
"""
Created on Sat Apr  7 19:26:03 2018

@author: rafael.araujo
"""

class normalizer:
    def org_it(csv_data):        
        #### data process to normalize values
        for index_row in range(1, len(csv_data)):
            #### fix data with wrong concatenated values
            if len(csv_data[0]) != len(csv_data[index_row]):
                for index_val in range(0, len(csv_data[index_row])):
                    if '\t' in csv_data[index_row][index_val]:
                        csv_data[
                          index_row][
                          index_val] = csv_data[
                                         index_row][
                                         index_val].replace('\t', ' \t ')
                        
                        new_list = csv_data[index_row][index_val].split('\t')
                        
                        csv_data[index_row] = csv_data[
                                                index_row][
                                                0 : index_val] + new_list + csv_data[
                                                                              index_row][
                                                                              index_val + 1 :]
            
            #### formating date values
            csv_data[index_row][34] = csv_data[index_row][34].replace('.','/')
            csv_data[index_row][37] = csv_data[index_row][37].replace('.','/')
            csv_data[index_row][41] = csv_data[index_row][41].replace('.','/')
            
            #### fix wrong values to birth date
            birth_date = csv_data[index_row][11]
            if '.' not in birth_date or int(birth_date[-4:]) < 1920:
                csv_data[index_row][11] = ''
                
        return csv_data
    
with open(full_path, newline = '\n') as csv_file:
    csv_data = list(reader(csv_file, delimiter = '\t'))
    csv_data[0] = csv_data[0][0].split(',')
    
normalized_csv = normalizer(csv_data)